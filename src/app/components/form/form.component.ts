import { Component, OnInit } from '@angular/core';
import { PopupService } from 'src/app/popup.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css'],
  providers: [PopupService]
})
export class FormComponent implements OnInit {
  hide:boolean = true;
  constructor(private popup: PopupService) { }

  ngOnInit( ): void {
  }
  submit(data): void {
    this.popup.setData({name:data.value.name,email:data.value.email});
    //console.log(data.value.name,data.value.email,data.value.password);
    data.reset();

  }
}
