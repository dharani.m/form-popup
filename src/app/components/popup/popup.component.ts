import { Component, OnInit } from '@angular/core';
import { PopupService } from 'src/app/popup.service';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.css']
})
export class PopupComponent implements OnInit {

  data = {name:"",email:""};
  isOpen = false;

  constructor(public popupData: PopupService) { }
  close(){
    this.isOpen = false;
    this.popupData.setData({name:"",email:""});
  }
  ngOnInit(): void {
  }
  getData(){
    let temp = this.popupData.getData();
    this.data = {
      name : temp.name,
      email : temp.email
    }
  }
}
