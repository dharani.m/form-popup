import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PopupService {
  data = {name:"",email:""};
  constructor() { }

  getData(){
    return { 
      name: this.data.name,
      email: this.data.email
    };
  }
  setData(data){
    console.log(data);
    this.data = data;
  }
  hasData(): boolean{
    return this.data.name!="" && this.data.email!="" ? true : false;
  }
}
